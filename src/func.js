const getSum = (str1, str2) => {
    if (Object.prototype.toString.call(str1) !== "[object String]" || Object.prototype.toString.call(str2) !== "[object String]") {
        return false;
    }
    let remainder = 0;
    let nums = {1: 1, 2: 2, 3: 3, 4: 4, 5: 5, 6: 6, 7: 7, 8: 8, 9: 9, 0: 0};
    str1 = str1.length === 0 ? "0" : str1;
    str2 = str2.length === 0 ? "0" : str2;
    if (str1.length > str2.length) {
        let temp = str2;
        str2 = str1;
        str1 = temp;
    }
    let res = [];
    for (let i = str1.length - 1, j = str2.length - 1; j > -1; i--, j--) {
        let sum = 0;
        if (i < 0) {
            sum = nums[str2[j]];
        } else {
            sum = nums[str1[i]] + nums[str2[j]];
        }
        sum += remainder;
        if (sum > 9) {
            remainder = sum - 9;
            sum %= 10;
        }
        if (isNaN(sum)) {
            return false;
        }
        res.push(sum);
    }
    if (remainder !== 0) {
        res.push(remainder);
    }
    res.reverse();
    return res.join("");
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
    let posts = 0, comments = 0;
    for (const post of listOfPosts) {
        if (post.author === authorName) {
            posts++;
        }
        if (post.comments === undefined) {
            continue
        }
        for (const comment of post.comments) {
            if (comment.author === authorName) {
                comments++;
            }
        }
    }
    return "Post:" + posts + ",comments:" + comments;
};

const tickets = (people) => {
    let change = [];
    for (const person of people) {
        change.push(person);
        if (person === 25) {
            continue;
        }
        if (person === 50 && change.indexOf(25) !== -1) {
            change.splice(change.indexOf(25), 1);
        } else if (person === 100 && change.indexOf(25) !== -1 && change.indexOf(50) !== -1) {
            change.splice(change.indexOf(25), 1);
            change.splice(change.indexOf(50), 1);
        } else {
            return "NO";
        }
    }
    return "YES";
};


module.exports = {getSum, getQuantityPostsByAuthor, tickets};
